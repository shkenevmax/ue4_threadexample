// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleAtomic_Runnable.h"

FSimpleAtomic_Runnable::FSimpleAtomic_Runnable(FColor Color, AThreadExampleGameModeBase *OwnerActor, uint32 NeedIteration)
{
	GameMode_ref = OwnerActor;
	NumberIteration = NeedIteration;
}

FSimpleAtomic_Runnable::~FSimpleAtomic_Runnable()
{
}

bool FSimpleAtomic_Runnable::Init()
{
	return true;
}

uint32 FSimpleAtomic_Runnable::Run()
{

	return 0;
}

void FSimpleAtomic_Runnable::Stop()
{
}

void FSimpleAtomic_Runnable::Exit()
{
}

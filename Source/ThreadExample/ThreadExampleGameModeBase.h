// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HAL/ThreadingBase.h"
#include "SynPrim/SimpleAtomic_Runnable.h"
#include "ThreadExampleGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class THREADEXAMPLE_API AThreadExampleGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	virtual void Tick(float) override;
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;

	// Simple atomic setting
	TArray<FRunnableThread*> CurrentRunnableGameModeThread_SimpleAtomic;

	// Simple atomic control
	UFUNCTION(BlueprintCallable)
	void CreateSimpleAtomicThread();

	// Simple atomic storage
	std::atomic_int16_t AtomicCounter1;
	int16 NonAtomicCounter1;
};

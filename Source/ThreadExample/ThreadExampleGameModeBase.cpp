// Copyright Epic Games, Inc. All Rights Reserved.


#include "ThreadExampleGameModeBase.h"

void AThreadExampleGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// FPlatformTLS - thread local storege (��������� ��������� ����� �� ����������)
	uint32 ThreadID = FPlatformTLS::GetCurrentThreadId();

	// FThreadManager - �������� �������, ������� ��������� ��� ��������� (���������, ������� � ��)
	UE_LOG(LogTemp, Error, TEXT("AThreadExampleGameModeBase::Tick ID - %d, Name - %s"), 
		ThreadID, *FThreadManager::GetThreadName(ThreadID));

	FThreadManager::Get().ForEachThread([&](uint32 ThreadID, FRunnableThread* Runnable)
		{
			FString myThreadType = "none";
			switch (Runnable->GetThreadPriority())
			{
			case TPri_Normal:
				myThreadType = "TPri_Normal";
				break;
			case TPri_AboveNormal:
				myThreadType = "TPri_AboveNormal";
				break;
			case TPri_BelowNormal:
				myThreadType = "TPri_BelowNormal";
				break;
			case TPri_Highest:
				myThreadType = "TPri_Highest";
				break;
			case TPri_Lowest:
				myThreadType = "TPri_Lowest";
				break;
			case TPri_SlightlyBelowNormal:
				myThreadType = "TPri_SlightlyBelowNormal";
				break;
			case TPri_TimeCritical:
				myThreadType = "TPri_TimeCritical";
				break;
			case TPri_Num:
				myThreadType = "TPri_Num";
				break;
			default:
				break;
			}
			UE_LOG(LogTemp, Error, TEXT("AThreadExampleGameModeBase::Tick ID - %d, Name - %s, Priority - %s"),
				ThreadID, *FThreadManager::GetThreadName(ThreadID), *myThreadType);
		});
}

void AThreadExampleGameModeBase::BeginPlay()
{
	Super::BeginPlay();
}

void AThreadExampleGameModeBase::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void AThreadExampleGameModeBase::CreateSimpleAtomicThread()
{
	FColor Color;
	class FSimpleAtomic_Runnable* MyRunnableClass_SimpleAtimic = new FSimpleAtomic_Runnable(Color, this, 2000);
	CurrentRunnableGameModeThread_SimpleAtomic.Add(FRunnableThread::Create(MyRunnableClass_SimpleAtimic, 
		TEXT("SimpleAtomic thread"), 0, EThreadPriority::TPri_Normal));
}
